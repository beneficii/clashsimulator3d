﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Mathematics;
using EpPathFinding.cs;

public static class CombatSimulationExtensions
{
    public static int MagnitudeSqr(this int2 v)
    {
        return v.x * v.x + v.y * v.y;
    }

    public static int Magnitude(this int2 v)
    {
        return (int)math.sqrt(v.MagnitudeSqr());
    }

    public static int2 MoveTowards(this int2 pos, int2 target, int maxDistance)
    {
        int2 diff = target - pos;
        int magnitude = diff.Magnitude();

        if (magnitude <= maxDistance)
        {
            return target;
        }
        else
        {
            return pos + diff * maxDistance / magnitude;
        }
    }

    public static int MagnitudeSqr(this int2[] path)
    {
        int result = 0;

        for (int i = 1; i < path.Length; i++)
        {
            result += (path[i] - path[i - 1]).MagnitudeSqr();
        }

        return result;
    }

    public static int Magnitude(this int2[] path)
    {
        int result = 0;

        for (int i = 1; i < path.Length; i++)
        {
            result += (path[i] - path[i - 1]).Magnitude();
        }

        return result;
    }

    public static GridPos ToJpGridPos(this int2 v)
    {
        return new GridPos(v.x, v.y);
    }

    public static int2 ToInt2(this GridPos v)
    {
        return new int2(v.x, v.y);
    }

    public static bool NotGridNull (this int2 v) // hack for pathfinding search
    {
        return v.x > -10;
    }

    public static int Sqr(this int n)
    {
        return n * n;
    }
}

public static class PathUtils
{
    public static IEnumerable<int2> GetLine(int2 from, int2 to)
    {
        int dx = Mathf.Abs(to.x - from.x), sx = from.x < to.x ? 1 : -1;
        int dy = Mathf.Abs(to.y - from.y), sy = from.y < to.y ? 1 : -1;
        int err = (dx > dy ? dx : -dy) / 2, e2;
        while (true)
        {
            yield return from;
            if (from.x == to.x && from.y == to.y) break;
            e2 = err;
            if (e2 > -dx) { err -= dy; from.x += sx; }
            if (e2 < dy) { err += dx; from.y += sy; }
        }
    }
}