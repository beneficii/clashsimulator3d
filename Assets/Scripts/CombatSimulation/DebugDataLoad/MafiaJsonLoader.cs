﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Mathematics;
using CombatSimulation;

public class MafiaJsonLoader : MonoBehaviour
{
    public static Dictionary<int, int2> data;

    public TextAsset jsonData;
    public TextAsset jsonLevel;

    public UnitBaseStats baseStats;

    [System.Serializable]
    public class BList
    {
        public List<BData> list;
    }

    [System.Serializable]
    public class BData
    {
        public int id;
        public int sizeX;
        public int sizeY;
        public int position;
    }

    public IEnumerable<LevelData.PixelData> LoadLevel()
    {
        data = new Dictionary<int, int2>();
        foreach (var item in JsonUtility.FromJson<BList>(jsonData.text).list)
        {
            data.Add(item.id, new int2(item.sizeX, item.sizeY));
        }


        var level = JsonUtility.FromJson<BList>(jsonLevel.text).list;

        foreach (var item in level)
        {
            var size = data[item.id];

            var stats = baseStats;
            stats.stats.size = size;

            yield return new LevelData.PixelData(new int2(item.position/46, item.position%46), stats);
        }


    }
}
