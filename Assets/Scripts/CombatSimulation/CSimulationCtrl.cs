﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Mathematics;
using EpPathFinding.cs;
using System.Linq;
using CombatSimulation;


public class CSimulationCtrl : MonoBehaviour
{
    public const int ChunkSize = 100;
    public const int GroupOffset = 1000000; //used as an index offset when we mix units and building together

    public bool visualize;
    PathFinding _pathfinding;
    public GameObject debugPrefab;


    public LevelData level;

    //objects and data
    List<CsUnit> _units;
    List<CsBuilding> _buildings;

    ISimulationTarget GetTarget(int idx)
    {
        var i = idx % GroupOffset;
        switch ((CsGroupType)(idx/GroupOffset))
        {
            case CsGroupType.AllyUnit: return _units[i];
            case CsGroupType.EnemyBuilding: return _buildings[i];
            default: throw new KeyNotFoundException();
        }
    }

    CsGroupType TargetGroupType(int idx)
    {
        return (CsGroupType)(idx / GroupOffset);
    }

    int GetOffsetIdx(int idx, CsGroupType type)
    {
        return (int)type * GroupOffset + idx;
    }

    int _totalAllyUnits;
    int _totalEnemeies;


    WorldObjectVisualizerBase[] _visualsUnits;
    WorldObjectVisualizerBase[] _visualsBuildings;

    int _step;
    Dictionary<int, List<CsActionData>> _actionQueue;
    List<KeyValuePair<int, CsPathRequest>> _pathRequests;

    //debug stuff
    float _startTime;
    public List<Material> materials;
    public WorldObjectVisualizerBase prefabVisuals;


    private void Start()
    {
        Init();

        if(!visualize)
        {
            JustCalculate();
        }
    }

    void Init()
    {
        ResetField();

        _startTime = Time.realtimeSinceStartup;
    }

    void ResetField()
    {
        _step = 0;

        _actionQueue = new Dictionary<int, List<CsActionData>>();
        _pathfinding = new PathFinding(50, 50);
        _pathfinding.prefabDebugPath = debugPrefab;
        _totalAllyUnits = 0;
        _totalEnemeies = 0;

        _units = new List<CsUnit>();
        _buildings = new List<CsBuilding>();

        foreach (var pixel in level.LoadLevel())
        //foreach (var pixel in GetComponent<MafiaJsonLoader>().LoadLevel())
        {
            if (pixel.stats.isBuilding)
            {
                var building = new CsBuilding(pixel);
                _pathfinding.SetWalkable(building, GroupOffset + _buildings.Count, false);
                _buildings.Add(building);
            }
            else
            {
                _units.Add(new CsUnit(pixel));
            }

            switch (pixel.stats.team)
            {
                case TeamType.Friendly:
                    if(!pixel.stats.isBuilding)_totalAllyUnits++;
                    break;
                case TeamType.Enemy:
                    if (pixel.stats.stats.priorityType != CsPriorityType.Wall) _totalEnemeies++;
                    break;
                default:
                    break;
            }
        }

        //init visuals
        if (_visualsBuildings == null)
        {
            var visualsList = new List<WorldObjectVisualizerBase>();
            foreach (var item in this._buildings)
            {
                var instance = Instantiate(prefabVisuals);
                visualsList.Add(instance);
             
            }
            _visualsBuildings = visualsList.ToArray();

            visualsList = new List<WorldObjectVisualizerBase>();
            foreach (var item in _units)
            {
                var instance = Instantiate(prefabVisuals);
                visualsList.Add(instance);
            }
            _visualsUnits = visualsList.ToArray();
        }

        for (int i = 0; i < this._buildings.Count; i++)
        {
            _visualsBuildings[i].Init(materials[this._buildings[i].stats.id]);
            _visualsBuildings[i].Refresh(this._buildings[i]);
        }

        for (int i = 0; i < _units.Count; i++)
        {
            _visualsUnits[i].Init(materials[_units[i].stats.id]);
            _visualsUnits[i].Refresh(_units[i]);
        }
    }

    void AddToQueue(CsActionData action, int step)
    {
        if(!_actionQueue.ContainsKey(step))
        {
            _actionQueue.Add(step, new List<CsActionData>());
        }

        _actionQueue[step].Add(action);
    }

    int2[] FindPath(CsPathRequest from, ISimulationTarget to)
    {
        return _pathfinding.Find(from, to);
    }

    //ignores buildings and walls on the way
    CsTargetAndPath FindLastWallInPath(CsPathRequest from, ISimulationTarget to)
    {
        foreach (var pos in PathUtils.GetLine(to.GridPosition, from.gridPosition))
        {
            var idx = _pathfinding.GetBuildingAt(pos);
            if (idx == -1) continue;

            var target = GetTarget(idx);
            if (target.PriorityType != CsPriorityType.Wall) continue;

            var path = FindPath(from, target);

            if (path.Length > 0)
            {
                return new CsTargetAndPath(idx, path);
            }
        }

        return CsTargetAndPath.Null;
    }

    CsTargetAndPath FindTargetAndPath(CsPathRequest data, int targetCount)
    {
        var targets = new List<CsTargetAndIndex>();
        bool foundPriority = false;

        bool GoodTarget(ISimulationTarget t)
        {
            if (t.IsAlive && t.Team != data.sourceTeam && t.PriorityType != CsPriorityType.Untargetable)
            {
                if (t.PriorityType == data.priority)
                {
                    if (data.priority != CsPriorityType.None)
                    {
                        foundPriority = true;
                    }
                    return true;
                }
                else
                {
                    return !foundPriority && t.PriorityType != CsPriorityType.Wall;
                }
            }
            else
            {
                return false;
            }
        }

        for (int i = 0; i < _units.Count; i++)
        {
            var target = _units[i];
            if (GoodTarget(target))
            {
                targets.Add(new CsTargetAndIndex(target, i));
            }
        }

        for (int i = 0; i < _buildings.Count; i++)
        {
            var target = _buildings[i];
            if (GoodTarget(target))
            {
                targets.Add(new CsTargetAndIndex(target, GetOffsetIdx(i, CsGroupType.EnemyBuilding)));
            }
        }

        if (foundPriority) //only target priority enemies if any found
        {
            targets = targets.Where(x => x.target.PriorityType == data.priority).ToList();
        }

        if (targets.Count < 1) // no targets on the whole map
        {
            return CsTargetAndPath.Null;
        }


        var ordered = targets
                .OrderBy(x => (x.target.GridPosition - data.gridPosition).MagnitudeSqr()) // order by straigth distance
                .Take(targetCount);  // take closest N targets

        //calculate closest straight path
        var closestTarget = ordered.First();
        var straightPath = PathUtils.GetLine(data.gridPosition, closestTarget.target.GridPosition).ToArray();
        var wallInPath = FindLastWallInPath(data, closestTarget.target);
        if (wallInPath.targetIdx == -1) //no walls in our path, let's go
        {
            return new CsTargetAndPath(closestTarget.targetIdx, FindPath(data, closestTarget.target));
        }
        else if (wallInPath.targetIdx == closestTarget.targetIdx) //means wall is our priority
        {
            return wallInPath;
        }


        var result = ordered
                .Select(x => new CsTargetAndPath(x.targetIdx, FindPath(data, x.target))) //calculate path for each found target
                .Where(x => x.path.Length > 0) //filter out paths that were not found
                .OrderBy(x => x.path.MagnitudeSqr()); // order by distance of paths


        //means there are targets, but no path to reach them
        if (result.Count() < 1) return wallInPath;

        var firstPath = result.First();
        int wallPathBonus = 10;

        //var firstPathMagnitude = straightPath.Magnitude();
        if (firstPath.path.Magnitude() > straightPath.Magnitude() + wallPathBonus)
        {
            return wallInPath;
        }
        else
        {
            return firstPath;
        }
    }

    int FindBuildingTarget(CsBuilding building)
    {
        var rangeSqr = building.stats.RangeSqr;
        var gridPos = building.GridPosition;

        for (int i = 0; i < _units.Count; i++)
        {
            var target = _units[i];
            if (target.IsAlive && target.team != building.team
                && (gridPos - target.GridPosition).MagnitudeSqr() <= rangeSqr)
            {
                return i;
            }
        }

        return -1;
    }


    void ApplyTargetAction(CsActionData action)
    {
        if(action.target < GroupOffset)
        {
            var target = _units[action.target];

            if(!target.IsAlive)
            {
                return;
            }

            switch (action.type)
            {
                case CsActionData.Type.Damage:
                    target.hp -= action.value;
                    
                    break;
                case CsActionData.Type.Healing:
                    target.hp += action.value;
                    break;
                default:
                    break;
            }

            if (!target.IsAlive) //means he just died
            {
                switch (target.team)
                {
                    case TeamType.Friendly:
                        _totalAllyUnits--;
                        break;
                    case TeamType.Enemy:
                        _totalEnemeies--;
                        break;
                    default:
                        break;
                }

            }
            _units[action.target] = target;
        } else
        {
            var target = _buildings[action.target % GroupOffset];

            if (!target.IsAlive)
            {
                return;
            }

            switch (action.type)
            {
                case CsActionData.Type.Damage:
                    target.hp -= action.value;

                    break;
                case CsActionData.Type.Healing:
                    target.hp += action.value;
                    break;
                default:
                    break;
            }

            if (!target.IsAlive) //means he just died
            {
                _pathfinding.SetWalkable(target, action.target, true);

                switch (target.team)
                {
                    case TeamType.Friendly:
                        //_totalAllyUnits--;
                        break;
                    case TeamType.Enemy:
                        if (target.stats.priorityType != CsPriorityType.Wall) _totalEnemeies--;
                        break;
                    default:
                        break;
                }

            }
            _buildings[action.target - GroupOffset] = target;
        }
    }

    void ApplyVisualAction(CsActionData action)
    {
        WorldObjectVisualizerBase target;
        if (action.target < GroupOffset)
        {
            target = _visualsUnits[action.target];
        } else
        {
            target = _visualsBuildings[action.target % GroupOffset];
        }

        WorldObjectVisualizerBase source;

        if (action.source < GroupOffset)
        {
            source = _visualsUnits[action.source];
        }
        else
        {
            source = _visualsBuildings[action.source%GroupOffset];
        }


        switch (action.type)
        {
            case CsActionData.Type.SpawnProjectile:
                source.SpawnProjectile(target.transform, action.value);
                break;
            default:
                break;
        }

    }


    void CalculateUnitsStep()
    {
        _pathRequests = new List<KeyValuePair<int, CsPathRequest>>();

        for (int i = 0; i < _units.Count; i++)
        {
            var unit = _units[i];

            if (!unit.IsAlive) continue;

            if(unit.HasTarget && !GetTarget(unit.target).IsAlive) // if unit is dead, target is lost
            {
                unit.target = -1;
            }

            if(!unit.HasTarget) // if no target, find a new one
            {
                _pathRequests.Add(new KeyValuePair<int, CsPathRequest>(i, new CsPathRequest()
                {
                    gridPosition = unit.GridPosition,
                    rangeSqr = unit.stats.RangeSqr,
                    priority = unit.stats.attackPriority,
                    sourceTeam = unit.team
                }));
            }
            else if(unit.waypointIdx >= unit.waypoints.Length) // path finished
            {
                if (_step >= unit.nextAttack)
                {
                    if (visualize)
                    {
                        AddToQueue(CsActionData.Projectile(i, unit.target, 20), _step);
                    }
                    AddToQueue(CsActionData.Damage(unit.target, unit.stats.damagePerHit), _step + 20);
                    unit.nextAttack = _step + unit.stats.attackDelay;
                }
            }
            else //not the end of the path yet, keep walking
            {
                var targetPosition = unit.NextWaypoint;
                unit.position = unit.position.MoveTowards(targetPosition, unit.stats.MovementSpeed);
                if (unit.position.Equals(targetPosition))
                {
                    unit.waypointIdx++;
                }
            }

            _units[i] = unit;
        }
    }

    void CalculateBuildingsStep()
    {
        for (int i = 0; i < _buildings.Count; i++)
        {

            var building = _buildings[i];

            if (!building.IsAlive) continue;

            if (_step >= building.nextAttack)
            {
                if (!building.HasTarget || !GetTarget(building.target).IsAlive)
                {
                    building.target = FindBuildingTarget(building);
                }

                if (building.HasTarget)
                {
                    if ((building.GridPosition - GetTarget(building.target).GridPosition).MagnitudeSqr() <= building.stats.RangeSqr) // target still in range
                    {
                        if(visualize)
                        {
                            AddToQueue(CsActionData.Projectile(GetOffsetIdx(i, CsGroupType.EnemyBuilding), building.target, 20), _step);
                        }
                        AddToQueue(CsActionData.Damage(building.target, building.stats.damagePerHit), _step + 20);
                    }
                    else // target got out of range, let's search another one
                    {
                        building.target = -1;
                    }
                }

                building.nextAttack = _step + building.stats.attackDelay;
            }
            _buildings[i] = building;
        }
    }

    //damage is calculated only here, even if unit kills itself
    void CalculateQueueStep() 
    {
        if (!_actionQueue.ContainsKey(_step)) return;

        foreach (var action in _actionQueue[_step])
        {
            ApplyTargetAction(action);
        }

        if(visualize)
        {
            foreach (var action in _actionQueue[_step])
            {
                ApplyVisualAction(action);
            }
        }

        _actionQueue.Remove(_step); // free up some memory
    }


    void CalculatePathSearchStep()
    {
        var pathDict = new Dictionary<CsPathRequest, CsTargetAndPath>();

        foreach (var request in _pathRequests)
        {
            var unit = _units[request.Key];
            if(pathDict.TryGetValue(request.Value, out var value))
            {
                unit.target = value.targetIdx;
                unit.waypoints = value.path;
                unit.waypointIdx = 1;
                
            } else
            {
                var targetAndPath = FindTargetAndPath(request.Value, 3);
                unit.target = targetAndPath.targetIdx;
                unit.waypoints = targetAndPath.path;
                unit.waypointIdx = 1;
                pathDict.Add(request.Value, targetAndPath);
            }

            _units[request.Key] = unit;
        }


        /*
        var targetAndPath = FindTargetAndPath(unit, 3);
        unit.target = targetAndPath.targetIdx;
        unit.waypoints = targetAndPath.path;
        unit.waypointIdx = 1;*/
    }

    void DrawUnitsStep()
    {
        for (int i = 0; i < _units.Count; i++)
        {
            _visualsUnits[i].Refresh(_units[i]);
        }
    }

    void DrawBuildingsStep()
    {
        for (int i = 0; i < _buildings.Count; i++)
        {
            _visualsBuildings[i].Refresh(_buildings[i]);
        }
    }

    bool combatFinsihed = false;
    void SimulationStep()
    {
        CalculateUnitsStep();
        CalculateBuildingsStep();

        CalculateQueueStep();

        CalculatePathSearchStep();

        if(visualize)
        {
            DrawUnitsStep();
            DrawBuildingsStep();
        }

        if(_step > 300*50 || _totalAllyUnits <= 0 || _totalEnemeies <= 0)
        {
            Debug.Log($"objects {_units.Count} + {_buildings.Count}: step {_step}, time {(Time.realtimeSinceStartup - _startTime) * 1000:#.00}ms");
            combatFinsihed = true;
        }

        _step++;
    }

    void FixedUpdate()
    {
        if (!visualize) return;

        if (!combatFinsihed)
        {
            SimulationStep();
        }
    }

    void JustCalculate()
    {
        while (!combatFinsihed)
        {
            SimulationStep();
        }
    }

    #region pathfinding class
    [System.Serializable]
    public class PathFinding
    {
        BaseGrid _searchGrid;
        JumpPointParam _jpParam;
        Dictionary<int2, int> _buildings; //pos=>type

        public GameObject prefabDebugPath;

        int _width, _height;

        public PathFinding(int width, int height)
        {
            _width = width;
            _height = height;
            _searchGrid = new StaticGrid(width, height);
            for (int x = 0; x < width; x++)
            {
                for (int y = 0; y < height; y++)
                {
                    _searchGrid.SetWalkableAt(x, y, true);
                }
            }

            _jpParam = new JumpPointParam(_searchGrid, EndNodeUnWalkableTreatment.DISALLOW);
            _buildings = new Dictionary<int2, int>();
        }

        public int GetBuildingAt(int2 pos)
        {
            if(_buildings.TryGetValue(pos, out var value))
            {
                return value;
            }

            return -1;
        }

        public void SetWalkable(int2 pos, bool value)
        {
            _searchGrid.SetWalkableAt(pos.x, pos.y, value);
        }

        public void SetWalkable(CsBuilding building, int index, bool value)
        {
            foreach (var pos in building.BlockingChunks)
            {
                if(!value)
                {
                    if(!_buildings.ContainsKey(pos)) //if someone messes up building placement
                    {
                        _buildings.Add(pos, index);
                    }

                } else
                {
                    _buildings.Remove(pos);
                }

                SetWalkable(pos, value);
            }
        }

        public int2[] Find(CsUnit unit, ISimulationTarget target)
        {
            _jpParam.SetMafiaParam(unit, target);
            //return new int2[1] { finish };
            return JumpPointFinder.FindPath(_jpParam).Select(v => v.ToInt2()).ToArray();
        }

        public int2[] Find(CsPathRequest source, ISimulationTarget target)
        {
            _jpParam.SetMafiaParam(source, target);
            //return new int2[1] { finish };
            return JumpPointFinder.FindPath(_jpParam).Select(v => v.ToInt2()).ToArray();
        }
    }
    #endregion
}
