﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    Transform _target;
    float _speed;


    public void Init(Transform target, int stepsTillReach)
    {
        _target = target;

        float reachTime = stepsTillReach * Time.fixedDeltaTime;

        _speed = (transform.position - target.position).magnitude / reachTime;
        //_speed = 1f;

    }

    private void FixedUpdate()
    {
        var target = _target.position + Vector3.up * 0.5f;
        transform.position = Vector3.MoveTowards(transform.position, target, _speed * Time.fixedDeltaTime);

        if(transform.position == target)
        {
            Destroy(gameObject);
            enabled = false; //to prevent any more updates
        }
    }
}
