﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using CombatSimulation;
using Unity.Mathematics;

public class WorldObjectVisualizerBase : MonoBehaviour
{
    //public TextMeshProUGUI txtHealth;
    public Image imgHealth;
    public Renderer render;

    public Material deadMaterial;

    public Bullet prefabBullet;

    bool _isDead;
    bool _forceRefresh = false;

    CsUnit _cachedUnitData;
    CsBuilding _cachedBuildingData;

    public void Refresh(CsUnit unit)
    {
        if (_isDead) return;

        if(unit.hp != _cachedUnitData.hp || _forceRefresh)
        {
            if (!unit.IsAlive)
            {
                OnDie();
            } else
            {
                //txtHealth.SetText($"{unit.hp}/{unit.stats.maxHp}");
                imgHealth.fillAmount = unit.hp / (float)unit.stats.maximumHealth;
            }
        }

        if(_forceRefresh)
        {
            transform.localScale = new Vector3(0.6f, 1.4f, 0.6f);
        }

        if (!unit.position.Equals(_cachedUnitData.position) || _forceRefresh)
        {
            transform.position = new Vector3(unit.position.x, 0, unit.position.y) / CSimulationCtrl.ChunkSize;
        }

        _cachedUnitData = unit;
        _forceRefresh = false;

    }

    public void Refresh(CsBuilding building)
    {
        if (_isDead) return;

        if (building.hp != _cachedBuildingData.hp || _forceRefresh)
        {
            if (!building.IsAlive)
            {
                OnDie();
            } else
            {
                //txtHealth.SetText($"{unit.hp}/{unit.stats.maxHp}");
                imgHealth.fillAmount = building.hp / (float)building.stats.maximumHealth;
            }
        }

        if (_forceRefresh) //set building dimensions
        {
            transform.localScale = new Vector3(building.stats.size.x, 1, building.stats.size.y);
        }

        if (!building.position.Equals(_cachedBuildingData.position) || _forceRefresh)
        {
            var corner = new Vector3(building.position.x, 0, building.position.y) / CSimulationCtrl.ChunkSize + new Vector3(building.stats.size.x - 1,0,building.stats.size.y-1)/2;

            //var corner = new Vector3(building.position.x / (float)CSimulationCtrl.ChunkSize, 0, building.position.y / (float)CSimulationCtrl.ChunkSize);
            transform.position = corner;
            //transform.position = corner + new Vector3(transform.localScale.x, 0, -transform.localScale.z) * 0.5f + new Vector3(0.5f, 0, 0.5f);
        }

        _cachedBuildingData = building;
        _forceRefresh = false;
    }

    void OnDie()
    {
        render.material = deadMaterial;
        //txtHealth.SetText($"");
        imgHealth.fillAmount = 0;
        _isDead = true;
        var scale = transform.localScale;
        scale.y = 0.1f;

        transform.localScale = scale;
    }

    public void Init(Material material)
    {
        _isDead = false;
        _forceRefresh = true;

        render.material = material;
    }


    public void SpawnProjectile(Transform target, int steps)
    {
        Instantiate(prefabBullet, transform.position + Vector3.up * 0.5f, Quaternion.identity).Init(target, steps);
    }
}
