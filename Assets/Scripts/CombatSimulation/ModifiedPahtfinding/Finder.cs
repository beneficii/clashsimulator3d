﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Mathematics;
/*
public class Finder : MonoBehaviour
{
    const int STRAIGHT_COST = 10;
    const int DIAGONAL_COST = 14;


    bool[][] _blocked;
    int _width;
    int _height;

    bool IsWalkable(int2 pos)
    {
        return IsWalkable(pos.x, pos.y);
    }

    bool IsWalkable(int x, int y)
    {
        if (x < 0 || x >= _width ||
           y < 0 || y >= _height)
        {
            return false;
        }

        return !_blocked[x][y];
    }

    static int Distance(int2 a, int2 b)
    {
        var delta = math.abs(a - b);

        return math.abs(delta.x - delta.y) * STRAIGHT_COST + math.min(delta.x, delta.y) * DIAGONAL_COST;
    }


    public static List<GridPos> FindPath(JumpPointParam iParam)
    {

        IntervalHeap<Node> tOpenList = iParam.openList;
        Node tStartNode = iParam.StartNode;
        Node tEndNode = iParam.EndNode;
        Node tNode;
        bool revertEndNodeWalkable = false;

        // set the `g` and `f` value of the start node to be 0
        tStartNode.startToCurNodeLen = 0;
        tStartNode.heuristicStartToEndLen = 0;

        // push the start node into the open list
        tOpenList.Add(tStartNode);
        tStartNode.isOpened = true;

        if (iParam.CurEndNodeUnWalkableTreatment == EndNodeUnWalkableTreatment.ALLOW && !iParam.SearchGrid.IsWalkableAt(tEndNode.x, tEndNode.y))
        {
            iParam.SearchGrid.SetWalkableAt(tEndNode.x, tEndNode.y, true);
            revertEndNodeWalkable = true;
        }

        // while the open list is not empty
        while (tOpenList.Count > 0)
        {
            // pop the position of node which has the minimum `f` value.
            tNode = tOpenList.DeleteMin();
            tNode.isClosed = true;

            if (tNode.Equals(tEndNode))
            {
                if (revertEndNodeWalkable)
                {
                    iParam.SearchGrid.SetWalkableAt(tEndNode.x, tEndNode.y, false);
                }
                return Node.Backtrace(tNode); // rebuilding path
            }

            identifySuccessors(iParam, tNode);
        }

        if (revertEndNodeWalkable)
        {
            iParam.SearchGrid.SetWalkableAt(tEndNode.x, tEndNode.y, false);
        }

        // fail to find the path
        return new List<GridPos>();
    }

    private static void identifySuccessors(JumpPointParam iParam, Node iNode)
    {
        HeuristicDelegate tHeuristic = iParam.HeuristicFunc;
        IntervalHeap<Node> tOpenList = iParam.openList;
        int tEndX = iParam.EndNode.x;
        int tEndY = iParam.EndNode.y;
        GridPos tNeighbor;
        GridPos tJumpPoint;
        Node tJumpNode;

        List<GridPos> tNeighbors = findNeighbors(iParam, iNode);
        for (int i = 0; i < tNeighbors.Count; i++)
        {
            tNeighbor = tNeighbors[i];
            if (iParam.CurIterationType == IterationType.RECURSIVE)
                tJumpPoint = jump(iParam, tNeighbor.x, tNeighbor.y, iNode.x, iNode.y);
            else
                tJumpPoint = jumpLoop(iParam, tNeighbor.x, tNeighbor.y, iNode.x, iNode.y);
            if (tJumpPoint != null)
            {
                tJumpNode = iParam.SearchGrid.GetNodeAt(tJumpPoint.x, tJumpPoint.y);
                if (tJumpNode == null)
                {
                    if (iParam.EndNode.x == tJumpPoint.x && iParam.EndNode.y == tJumpPoint.y)
                        tJumpNode = iParam.SearchGrid.GetNodeAt(tJumpPoint);
                }
                if (tJumpNode.isClosed)
                {
                    continue;
                }
                // include distance, as parent may not be immediately adjacent:
                float tCurNodeToJumpNodeLen = tHeuristic(Math.Abs(tJumpPoint.x - iNode.x), Math.Abs(tJumpPoint.y - iNode.y));
                float tStartToJumpNodeLen = iNode.startToCurNodeLen + tCurNodeToJumpNodeLen; // next `startToCurNodeLen` value

                if (!tJumpNode.isOpened || tStartToJumpNodeLen < tJumpNode.startToCurNodeLen)
                {
                    tJumpNode.startToCurNodeLen = tStartToJumpNodeLen;
                    tJumpNode.heuristicCurNodeToEndLen = (tJumpNode.heuristicCurNodeToEndLen == null ? tHeuristic(Math.Abs(tJumpPoint.x - tEndX), Math.Abs(tJumpPoint.y - tEndY)) : tJumpNode.heuristicCurNodeToEndLen);
                    tJumpNode.heuristicStartToEndLen = tJumpNode.startToCurNodeLen + tJumpNode.heuristicCurNodeToEndLen.Value;
                    tJumpNode.parent = iNode;

                    if (!tJumpNode.isOpened)
                    {
                        tOpenList.Add(tJumpNode);
                        tJumpNode.isOpened = true;
                    }
                }
            }
        }
    }

    private int2 jump(Settings iParam, int iX, int iY, int iPx, int iPy)
    {
        int2 current = new int2(iX, iY);


        if (!IsWalkable(iX, iY))
        {
            return new int2(-100,-100); //basically null
        }
        else if (iParam.TargetReached(current))
        {
            return current;
        }

        int tDx = iX - iPx;
        int tDy = iY - iPy;
        // check for forced neighbors
        // along the diagonal
        if (tDx != 0 && tDy != 0)
        {
            if ((IsWalkable(iX - tDx, iY + tDy) && !IsWalkable(iX - tDx, iY)) ||
                (IsWalkable(iX + tDx, iY - tDy) && !IsWalkable(iX, iY - tDy)))
            {
                return current;
            }
        }
        // horizontally/vertically
        else
        {
            if (tDx != 0)
            {
                // moving along x
                if ((IsWalkable(iX + tDx, iY + 1) && !IsWalkable(iX, iY + 1)) ||
                    (IsWalkable(iX + tDx, iY - 1) && !IsWalkable(iX, iY - 1)))
                {
                    return current;
                }
            }
            else
            {
                if ((IsWalkable(iX + 1, iY + tDy) && !IsWalkable(iX + 1, iY)) ||
                    (IsWalkable(iX - 1, iY + tDy) && !IsWalkable(iX - 1, iY)))
                {
                    return current;
                }
            }
        }
        // when moving diagonally, must check for vertical/horizontal jump points
        if (tDx != 0 && tDy != 0)
        {
            if (jump(iParam, iX + tDx, iY, iX, iY).NotGridNull())
            {
                return current;
            }
            if (jump(iParam, iX, iY + tDy, iX, iY).NotGridNull())
            {
                return current;
            }
        }

        return jump(iParam, iX + tDx, iY + tDy, iX, iY);
    }

    public struct Settings
    {
        public int rangeSqr;
        public int2 finish;

        public Settings(int2 finish, int range)
        {
            this.finish = finish;
            rangeSqr = range * range;
        }

        public bool TargetReached(int2 pos)
        {
            return (finish - pos).MagnitudeSqr() <= rangeSqr;
        }
    }

    public struct Costs
    {

    }
}
*/