﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Mathematics;

namespace CombatSimulation
{
    [System.Serializable]
    public struct CsUnit : ISimulationTarget
    {
        public int2 position;
        public int hp;
        public bool IsAlive => hp > 0;

        public int Health => hp;

        public int MaxHealth => stats.maximumHealth;

        public TeamType Team => team;

        public bool HasTarget => target != -1;

        public int2 GridPosition
        {
            get => position / CSimulationCtrl.ChunkSize;
            set => position = value * CSimulationCtrl.ChunkSize;
        }

        public CsWorldObjectStats stats;
        public CsWorldObjectStats Stats => stats;


        public int nextAttack;  //step at which next attack should be made

        public TeamType team;

        public int target; // -1 = none

        public int2[] waypoints;
        public int waypointIdx;
        public int2 NextWaypoint => waypoints[waypointIdx] * CSimulationCtrl.ChunkSize;

        public CsPriorityType PriorityType => CsPriorityType.Unit;

        public void RemoveHp(int amount)
        {
            hp -= amount;
        }

        public void AddHp(int amount)
        {
            hp += amount;
        }

        public CsUnit(LevelData.PixelData data)
        {
            position = data.pos * CSimulationCtrl.ChunkSize;
            stats = data.stats.stats;

            hp = stats.maximumHealth;
            nextAttack = stats.attackDelay;

            team = data.stats.team;
            target = -1;

            waypoints = new int2[0];
            waypointIdx = 1;
        }
    }

    [System.Serializable]
    public struct CsBuilding : ISimulationTarget
    {
        public int2 position;
        public int hp;


        public CsWorldObjectStats stats;
        public CsWorldObjectStats Stats => stats;


        public int nextAttack;  //step at which next attack should be made

        public TeamType team;

        public int target; // -1 = none

        public CsBuilding(LevelData.PixelData data)
        {
            position = data.pos * CSimulationCtrl.ChunkSize;
            stats = data.stats.stats;

            hp = stats.maximumHealth;
            nextAttack = stats.attackDelay;

            team = data.stats.team;
            target = -1;
        }

        public bool IsAlive => hp > 0;

        public IEnumerable<int2> BlockingChunks
        {
            get
            {
                int2 min = GridPosition;
                int2 max = min + stats.size - 1;

                if (stats.size.x > 1 && stats.size.y > 1)
                {
                    min += 1;
                    max -= 1;
                }

                for (int x = min.x; x <= max.x; x++)
                {
                    for (int y = min.y; y <= max.y; y++)
                    {
                        yield return new int2(x,y);
                    }
                }
            }
        }


        public int Health => hp;

        public int MaxHealth => stats.maximumHealth;

        public int2 GridPosition
        {
            get => position / CSimulationCtrl.ChunkSize;
            set => position = value * CSimulationCtrl.ChunkSize;
        }

        public TeamType Team => team;

        public bool HasTarget => target != -1;

        public CsPriorityType PriorityType => stats.priorityType;

        public void RemoveHp(int amount)
        {
            hp -= amount;
        }

        public void AddHp(int amount)
        {
            hp += amount;
        }
    }

    [System.Serializable]
    public struct CsWorldObjectStats
    {
        public int id;

        public int maximumHealth;
        public int damagePerHit;
        public int movementSpeed;
        public int attackRange;
        public int attackDelay;
        public int2 size;

        public int RangeSqr => attackRange * attackRange + 2; //+2 for good reasons
        public int MovementSpeed => movementSpeed * CSimulationCtrl.ChunkSize / 10;

        public CsPriorityType priorityType;
        public CsPriorityType attackPriority;
        public int priorityMultiplier;
    }

    public enum TeamType
    {
        Friendly,
        Enemy
    }

    public enum CsPriorityType
    {
        None,
        Resource,
        Defense,
        Army,
        Wall,
        Unit,
        Untargetable
    }


    public interface ISimulationTarget
    {
        //target search
        int2 GridPosition { get; set; }
        TeamType Team { get; }
        bool HasTarget { get; }

        // health
        void RemoveHp(int amount);
        void AddHp(int amount);
        int Health { get; }
        int MaxHealth { get; }
        bool IsAlive { get; }
        CsWorldObjectStats Stats { get; }
        CsPriorityType PriorityType { get; }

    }

    public struct CsActionData
    {
        public Type type;
        public int value;
        public int source;
        public int target;

        public static CsActionData Damage(int target, int value) //ToDo: add source
        {
            return new CsActionData()
            {
                type = Type.Damage,
                value = value,
                target = target
            };
        }

        public static CsActionData Projectile(int source, int target, int steps)
        {
            return new CsActionData()
            {
                type = Type.SpawnProjectile,
                source = source,
                target = target,
                value = steps
            };
        }


        public enum Type
        {
            Damage,
            Healing,
            SpawnProjectile
        }
    }

    //request to calculate path
    public struct CsPathRequest
    {
        public int2 gridPosition;
        public int rangeSqr;
        public CsPriorityType priority;
        public TeamType sourceTeam;
    }

    public struct CsTargetAndPath
    {
        public int targetIdx;
        public int2[] path;

        public CsTargetAndPath(int target, int2[] path)
        {
            this.targetIdx = target;
            this.path = path;
        }

        public static CsTargetAndPath Null => new CsTargetAndPath(-1, new int2[0]);
    }

    public struct CsTargetAndIndex
    {
        public int targetIdx;
        public ISimulationTarget target;

        public CsTargetAndIndex(ISimulationTarget target, int idx)
        {
            targetIdx = idx;
            this.target = target;
        }
    }

    public enum CsGroupType
    {
        AllyUnit = 0,
        EnemyBuilding = 1,
    }


}

