﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CombatSimulation;

[CreateAssetMenu(menuName = "Combat/Data/BaseStats")]
public class UnitBaseStats : ScriptableObject
{
    public CsWorldObjectStats stats;
    public TeamType team;

    public bool isBuilding = false;
}
