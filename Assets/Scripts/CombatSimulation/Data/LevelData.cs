﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Mathematics;


[CreateAssetMenu(menuName = "Combat/Data/Level")]
public class LevelData : ScriptableObject
{
    public Texture2D map;

    public UnitBaseStats redData;
    public UnitBaseStats greenData;
    public UnitBaseStats blueData;
    public UnitBaseStats tealData;
    public UnitBaseStats yellowData;


    public IEnumerable<PixelData> LoadLevel()
    {
        for (int x = 0; x < map.width; x++)
        {
            for (int y = 0; y < map.height; y++)
            {
                var pixel = map.GetPixel(x, y);
                if(pixel.r > 0.5f)
                {
                    if(pixel.g > 0.5f)
                    {
                        yield return new PixelData(new int2(x, y), yellowData);
                    }
                    else
                    {
                        yield return new PixelData(new int2(x, y), redData);
                    }

                } else if (pixel.g > 0.5f)
                {
                    if(pixel.b > 0.5f)
                    {
                        yield return new PixelData(new int2(x, y), tealData);
                    } else
                    {
                        yield return new PixelData(new int2(x, y), greenData);
                    }
                }
                else if(pixel.b > 0.5f)
                {
                    yield return new PixelData(new int2(x, y), blueData);
                }
            }
        }
    }

    public struct PixelData
    {
        public int2 pos;
        public UnitBaseStats stats;

        public PixelData(int2 pos, UnitBaseStats stats)
        {
            this.pos = pos;
            this.stats = stats;
        }
    }
}
